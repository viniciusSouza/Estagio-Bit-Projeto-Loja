package hitstore

import org.bson.types.ObjectId

class Chart {

    ObjectId id
    List<Product> productList = new ArrayList<>()
    String clientId

    static embedded = ['productList']

    static constraints = {
    }
}
