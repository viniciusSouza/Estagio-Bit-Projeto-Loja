package hitstore

import org.bson.types.ObjectId

class Order {

    ObjectId id
    String name
    String adress
    String chartId
    String status
    Double total
    String paymentType
    String deliverOption
    String clientId
    Map<String, Integer> productList = new HashMap<>()
    Date createdDate = new Date()

    static embedded = ['productList']

    static constraints = {
        name nullable: true
        adress nullable: true
        status nullable: true
        total nullable: true
        paymentType nullable: true
        deliverOption nullable: true
        clientId nullable: true
    }
}
