<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="layout" content="main"/>
        <title>Gerenciamento de produtos</title>
    </head>

    <body>
        <div class="container text-center">
            <h1>Gerenciamento de produtos</h1>
        </div>
        <div class="container">
            <a href="${g.createLink(action: 'create')}">
                <button type="button" class="btn-md btn-success">Adicionar Produto</button>
            </a>
        </div>
        <div class="container-fluid listOfProducts">
            <table>
                <tr>
                    <th>Imagem</th>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Status</th>
                    <th>Preço Total</th>
                    <th>Preço Desconto</th>
                    <th>Estoque</th>
                    <th>Criação</th>
                    <th>Atualização</th>
                </tr>
                <g:each in="${productList}">
                    <tr>
                        <td>
                            <img src="data:image/png;base64,${it.image.encodeBase64()}"/>
                        </td>
                        <td>${it.name}</td>
                        <td>${it.description}</td>
                        <td>${it.status}</td>
                        <td>${it.fullPrice}</td>
                        <td>${it.discountPrice}</td>
                        <td>${it.stock}</td>
                        <td>${it.createdDate}</td>
                        <td>${it.lastUpdated}</td>
                        <sec:ifAllGranted roles="ROLE_ADMIN">
                            <g:if test="${it.status}">
                                <td>
                                    <a href="${g.createLink(action: 'edit', id: it.id)}">
                                        <button type="button" class="btn-warning">Editar</button>
                                    </a>
                                </td>
                                <td>
                                    <a href="${g.createLink(action: 'delete', id: it.id)}">
                                        <button type="button" class="btn-danger">Deletar</button>
                                    </a>
                                </td>
                            </g:if>
                        </sec:ifAllGranted>
                    </tr>
                </g:each>
            </table>
        </div>
    </body>
</html>