<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="layout" content="main"/>
        <title>Formulário de Produto</title>
    </head>

    <body>

        <div class="container">
            <h1>${params.action == 'create' ? "Adicionar produto:" : "Editar produto:"}</h1>
        </div>
        <div class="container">
            <g:uploadForm url="[action: 'save', controller: 'product']" method="post">
                <g:if test="${product?.id}">
                    <input type="hidden" name="id" value="${product?.id}">
                </g:if>
                <div class="form-group">
                    <label for="name">Nome</label>
                    <g:textField name="name" id="name" value="${product?.name}" />
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <g:textField name="description" id="description" value="${product?.description}" />
                </div>
                <div class="form-group">
                    <label for="fullDescription">Descrição completa</label>
                    <g:textField name="fullDescription" id="fullDescription" value="${product?.fullDescription}" />
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <g:checkBox name="status" id="status" value="${true}" />
                </div>
                <div class="form-group">
                    <label for="fullPrice">Preço</label>
                    <g:field name="fullPrice" id="fullPrice" type="number" min="0" step="0.01" value="${product?.fullPrice}" />
                </div>
                <div class="form-group">
                    <label for="discountPrice">Preço com desconto</label>
                    <g:field name="discountPrice" id="discountPrice" type="number" min="0" step="0.01" value="${product?.discountPrice}" />
                </div>
                <g:if test="${params.action == 'create'}">
                    <div class="form-group">
                        <label for="image">Imagem</label>
                        <g:field name="image" id="image" type="file" value="" />
                    </div>
                </g:if>
                <g:else>
                    <button type="button" onclick="editImageToggle()" class="button btn btn-warning editImageBtn">Editar imagem</button>
                    <div class="form-group" id="editImageDiv">

                    </div>
                </g:else>
                <div class="form-group">
                    <label for="stock">Quantidade</label>
                    <g:field name="stock" id="stock" type="number" min="0" value="${product?.stock}" />
                </div>
                <g:submitButton name="submitButton" value="Salvar Produto" />
            </g:uploadForm>
        </div>

        <div class="container btnBack">
            <a href="${g.createLink(action: 'index')}">
                <button type="button" class="btn-primary">Voltar</button>
            </a>
        </div>

    </body>
</html>