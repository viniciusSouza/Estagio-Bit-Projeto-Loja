<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="layout" content="main"/>
        <title>Usuários</title>
    </head>

    <body>
    <div class="container formUser form-inline">
        <g:uploadForm url="[action: 'save', controller: 'user']" method="post">
            <div class="form-group form">
                <label for="username">Nome</label>
                <g:textField name="username" id = "username" />
            </div>
            <div class="form-group">
                <label for="password">Senha</label>
                <g:textField name="password" id = "password" />
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <g:field type="email" name="email" />
            </div>
            <div class="form-group">
                <label for="adress">Endereço</label>
                <g:textField name="adress" />
            </div>
            <g:submitButton name="submitButton" value="Cadastrar" />
        </g:uploadForm>
    </div>

    </body>
</html>