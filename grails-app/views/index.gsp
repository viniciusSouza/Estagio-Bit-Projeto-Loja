<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="layout" content="main"/>
        <title>Document</title>
    </head>

    <body>
        <div class="text-center">
            <h1>BEM VINDO A HIT STORE</h1>
        </div>
        <div class="container-fluid">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <a href="${g.createLink(action: 'productDetails' , controller: 'catalogo', params: [id: '5b08596a1649554b1632183f'])}">
                            <asset:image src="chinelo.jpg" />
                        </a>
                    </div>
                    <div class="item">
                        <a href="${g.createLink(action: 'productDetails' , controller: 'catalogo', params: [id: '5b0859d71649554b16321841'])}">
                            <asset:image src="talento.jpg" />
                        </a>
                    </div>
                    <div class="item">
                        <a href="${g.createLink(action: 'productDetails' , controller: 'catalogo', params: [id: '5b0859ab1649554b16321840'])}">
                            <asset:image src="cel.jpg" />
                        </a>
                    </div>
                </div>
        </div>
    </body>
</html>