<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="layout" content="main"/>
        <title>Checkout</title>
    </head>

    <body>
        <div class="container text-center">
            <h1>Pedido</h1>
        </div>

        <div class="container">
            <table>
                <tr>
                    <th>Imagem</th>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Preço Total</th>
                    <th>Preço Desconto</th>
                    <th>Estoque</th>
                </tr>
                <tr>
                    <td>
                        <img src="data:image/png;base64,${product.image.encodeBase64()}"/>
                    </td>
                    <td>${product.name}</td>
                    <td>${product.description}</td>
                    <td>${product.fullPrice}</td>
                    <td id="preco">${product.discountPrice}</td>
                    <td>${product.stock}</td>
                </tr>
            </table>
        </div>

        <div class="container fullDescriptionDiv">
            <p>Descrição completa:</p>
            <p>${product.fullDescription}</p>
        </div>

        <div class="container btnBack">
            <a href="${g.createLink(action: 'addToChart', params: [id: product.id])}">
                <button type="button" class="btn btn-primary">Adicionar no carrinho</button>
            </a>
        </div>

        <div class="container btnBack">
            <a href="${g.createLink(action: 'index')}">
                <button type="button" class="btn btn-primary">Voltar</button>
            </a>
        </div>


    </body>
</html>