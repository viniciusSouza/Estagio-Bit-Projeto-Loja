<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Checkout</title>
</head>

<body>
<div class="container text-center">
    <h1>Detalhes do produto</h1>
</div>

<div class="container productDetailImg">
    <img src="data:image/png;base64,${product.image.encodeBase64()}" class="center-block"/>
</div>

<div class="container">
    <table>
        <tr>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Preço Total</th>
            <th>Preço Desconto</th>
            <th>Estoque</th>
        </tr>
        <tr>
            <td>${product.description}</td>
            <td>${product.name}</td>
            <td>${product.fullPrice}</td>
            <td>${product.discountPrice}</td>
            <td>${product.stock}</td>
        </tr>
    </table>
</div>

<div class="container">
    <p>Detalhes do produto:</p>
    <p>${product.fullDescription}</p>
</div>

<div class="container btnBack">
    <a href="${g.createLink(action: 'index')}">
        <button type="button" class="btn btn-primary">Voltar</button>
    </a>

    <a href="${g.createLink(action: 'chart', id: product.id)}">
        <button type="button" class="btn btn-success">Comprar</button>
    </a>
</div>

</body>
</html>