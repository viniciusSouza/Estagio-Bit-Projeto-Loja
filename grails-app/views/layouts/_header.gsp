<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/#">
                <asset:image src="hitLogo.jpg" alt="Hit Logo" class="logo"/>
            </a>
        </div>
        <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;">
            <ul class="nav navbar-nav navbar-right menu">
                <g:pageProperty name="page.nav" />
                <li>
                    <a href="/#">Home</a>
                </li>
                <li>
                    <a href="${g.createLink(action: 'index' , controller: 'catalogo')}">Catalogo</a>
                </li>
                <sec:ifLoggedIn>
                    <sec:ifAllGranted roles="ROLE_ADMIN">
                        <li>
                            <a href="${g.createLink(action: 'index', controller: 'product')}">Gerenciamento de produtos</a>
                        </li>
                        <li>
                            <a href="${g.createLink(action: 'index' , controller: 'order')}">Histórico de pedidos</a>
                        </li>
                        <li>
                            <a href="/logout">Logout</a>
                        </li>
                    </sec:ifAllGranted>
                    <sec:ifAllGranted roles="ROLE_USER">
                        <li>
                            <a href="${g.createLink(action: 'index' , controller: 'chart')}">Carrinho</a>
                        </li>
                        <li>
                            <a href="${g.createLink(action: 'orderList' , controller: 'user')}">Pedidos</a>
                        </li>
                        <li class="loginName">
                            Bem vindo, <sec:username/>!
                        </li>
                        <li>
                            <a href="/logout">Logout</a>
                        </li>
                    </sec:ifAllGranted>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <li>
                        <a href="/login">Login</a>
                    </li>
                    <li>
                        <a href="${g.createLink(action: 'index' , controller: 'user')}">Cadastre-se</a>
                    </li>
                </sec:ifNotLoggedIn>
            </ul>
        </div>
    </div>
</div>

