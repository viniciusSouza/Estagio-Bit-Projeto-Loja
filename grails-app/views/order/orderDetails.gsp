<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Lista de pedidos</title>
</head>

<body>

    <div class="text-center">
        <h1>Detalhes do pedido: ${order.id}</h1>
    </div>

    <table>
        <tr>
            <th>Nome</th>
            <th>Endereço</th>
            <th>Data</th>
            <th>Forma de entrega</th>
            <th>Forma de pagamento</th>
            <th>Produtos</th>
            <th>Total</th>
        </tr>
        <tr>
            <td>${order.name}</td>
            <td>${order.adress}</td>
            <td>${order.createdDate}</td>
            <td>${order.deliverOption}</td>
            <td>${order.paymentType}</td>
            <td>
                <g:each in="${productList}">
                    ${it.key}
                    ${it.value};
                </g:each>
            </td>
            <td>${order.total}</td>
        </tr>
    </table>
</body>
</html>