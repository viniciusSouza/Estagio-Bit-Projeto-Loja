<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Lista de pedidos</title>
</head>

<body>

<div class="text-center">
    <h1>Histórico de Pedidos</h1>
</div>

<table>
    <tr>
        <th>IdPedido</th>
        <th>Nome</th>
        <th>Endereço</th>
        <th>Status</th>
        <th>Valor total</th>
    </tr>
    <g:each in="${orderList}">
        <tr>
            <td>
                <a href="${g.createLink(action: 'orderDetails', controller: 'order', id: it.id)}">${it.id}</a>
            </td>
            <td>${it.name}</td>
            <td>${it.adress}</td>
            <td>${it.status}</td>
            <td>${it.total}</td>
            <g:if test="${!it.status.equalsIgnoreCase('Finalizada') && !it.status.equalsIgnoreCase('Cancelada')}">
                <td>
                    <a href="${g.createLink(action: 'confirmOrder', controller: 'order', id: it.id)}">
                        <button type="button" class="btn btn-success">Confirmar</button>
                    </a>
                </td>
                <td>
                    <a href="${g.createLink(action: 'resendEmail', controller: 'order', id: it.id)}">
                        <button type="button" class="btn btn-success">Reenviar email</button>
                    </a>
                </td>
                <td>
                    <a href="${g.createLink(action: 'cancelOrder', controller: 'order', id: it.id)}">
                        <button type="button" class="btn btn-danger">Cancelar</button>
                    </a>
                </td>
            </g:if>
        </tr>
    </g:each>
</table>
</body>
</html>