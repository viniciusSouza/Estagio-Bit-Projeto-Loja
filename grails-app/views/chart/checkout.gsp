<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Lista de pedidos</title>
</head>

<body>

<div class="text-center">
    <h1>Carrinho</h1>
</div>


<table>
    <tr>
        <th>Produto</th>
        <th>Preço</th>
        <th>Quantidade</th>
        <th>Total</th>
    </tr>
    <g:each in="${productList}">
        <tr>
            <td>${it.name}</td>
            <td>${it.discountPrice}</td>
            <td>${order.productList.get((it.id).toString())}</td>
        </tr>
    </g:each>>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>${order.total}</td>
    </tr>
</table>

<div class="container">
    <g:uploadForm url="[action: 'checkout', controller: 'chart', id: order.id]" method="post">
        <input type="hidden" name="chartId" value="${order.id}">
        <div class="form-group">
            <label for="name">Nome</label>
            <g:textField name="name" id="name" value="${user?.username}"/>
        </div>
        <div class="form-group">
            <label for="adress">Endereço</label>
            <g:textField name="adress" id="adress" value="${user?.adress}"/>
        </div>
        <div class="form-group">
            <label for="paymentType">Forma de pagamento</label>
            <select name="paymentType">
                <option value="boleto">Boleto</option>
                <option value="creditCard">Cartão</option>
            </select>
        </div>
        <div class="form-group">
            <label for="deliverOption">Forma de entrega:</label>
            <select name="deliverOption">
                <option value="sedex">Sedex</option>
                <option value="correios">Correios</option>
            </select>
        </div>
        <g:submitButton name="submitButton" value="Finalizar pedido" />
    </g:uploadForm>

</div>
</body>
</html>