<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="layout" content="main"/>
    <title>Lista de pedidos</title>
</head>

<body>

<g:if test="${chart != null}">
    <div class="text-center">
        <h1>Carrinho</h1>
    </div>

    <table>
        <tr>
            <th>Produto</th>
            <th>Preço</th>
            <th>Quantidade</th>
            <th>Total</th>
        </tr>
        <tr>
            <g:uploadForm url="[action: 'confirmOrder', controller: 'chart', id: chart.id]" method="post">
                <g:each status="i" in="${chart.productList}" var="product">
                    <tr>
                        <td>${product.name}</td>
                        <td id="price">${product.discountPrice}</td>
                        <g:if test="${params.amount != null}">
                            <td>
                                <input type="number" name="amount" id="amount" max="${product.stock}" min="1" value="${params.amount[i]}" disabled>
                            </td>
                        </g:if>
                        <g:else>
                            <td>
                                <input type="number" name="amount" id="amount" max="${product.stock}" min="1" value="" required>
                            </td>
                        </g:else>
                        <g:if test="${params.amount == null}">
                            <td>
                                <a href="${g.createLink(action: 'removeItem', controller: 'chart', params: [chartId: chart.id, productId: product.id])}">
                                    <button type="button" class="btn btn-danger">Remover item</button>
                                </a>
                            </td>
                        </g:if>
                    </tr>
                </g:each>
                <g:if test="${params.amount == null}">
                    <tr>
                        <td>
                            <g:submitButton name="submitButton" value="Confirmar pedido" />
                        </td>
                    </tr>
                </g:if>
            </g:uploadForm>
        </tr>
    </table>

    <g:if test="${params.total != null}">
        <div class="container totalField">
            Total: <input type="number" step="0.01" id="total" disabled value="${params.total}">
        </div>
    </g:if>

    <g:if test="${params.amount != null}">
        <div class="container btnChart">
            <a href="${g.createLink(action: 'checkoutForm', controller: 'chart', id: params.orderId)}">
                <button type="submit" class="btn btn-success">Finalizar pedido</button>
            </a>
        </div>
        <div class="container btnChart">
            <a href="${g.createLink(action: 'cancelOrder', controller: 'chart', id: params.orderId)}">
                <button type="button" class="btn btn-danger">Cancelar pedido</button>
            </a>
        </div>
        <div class="container btnChart">
            <a href="${g.createLink(action: 'editOrder', controller: 'chart', id: params.orderId)}">
                <button type="button" class="btn btn-warning">Alterar pedido</button>
            </a>
        </div>
    </g:if>
</g:if>
<g:else>
    <h1>Carrinho vazio!</h1>
</g:else>

</body>
</html>