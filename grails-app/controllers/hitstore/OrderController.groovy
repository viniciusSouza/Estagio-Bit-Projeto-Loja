package hitstore

import grails.plugin.springsecurity.annotation.Secured
import hitStore.auth.User

class OrderController {

    @Secured(['ROLE_ADMIN'])
    def index() {

        def orderList = Order.list()

        render view: 'index', model: [orderList: orderList]
    }

    @Secured(['ROLE_ADMIN'])
    def cancelOrder(Order order) {

        def product = new Product()
        for (elem in order.productList) {
            product = Product.findById(elem.key)
            product.stock += elem.value
            product.save()
        }
        order.status = 'Cancelada'
        order.save()

        redirect action: 'index'
    }

    @Secured(['ROLE_ADMIN'])
    def confirmOrder(Order order) {

        order.status = "Finalizada"
        order.save()

        redirect action: 'index'
    }

    @Secured(['ROLE_ADMIN'])
    def resendEmail(Order order) {

        def user = User.findById(order.clientId)
        def product = new Product()

        StringBuilder listOfProductsForEmail = new StringBuilder("\n")
        for (elem in order.productList) {
            product = Product.findById(elem.key)
            listOfProductsForEmail.append(elem.value + " " + product.name  + "\n")
        }

        sendMail {
            from "testeestagiobit@gmail.com"
            to user.email
            subject "Reenvio: Nova compra " + order.id
            text ("Olá, " + user.username + "!" +
                    "\n" + "Sua compra de id: " + order.id + " está aguardando a confirmação de pagamento." + "\n" +
                    "Detalhes da compra: " + "\n" +
                    "Número do pedido: " + order.id + "\n" +
                    "Produtos: "  + listOfProductsForEmail +
                    "Valor total: " + order.total)
        }

        redirect action: 'index'
    }

    @Secured(['ROLE_USER', 'ROLE_ADMIN'])
    def orderDetails(Order order) {

        Map<String, Integer> productList = new HashMap<>()
        def product = new Product()
        for (elem in order.productList) {
            product = Product.findById(elem.key)
            productList.put(product.name, elem.value)
        }

        render view: 'orderDetails', model: [order: order, productList: productList]
    }
}
