package hitstore

import grails.plugin.springsecurity.annotation.Secured

@Secured (['ROLE_USER'])
class ChartController {

    def index() {

        def user = this.getAuthenticatedUser()
        def chart = Chart.findByClientId(user.id)

        render view: 'index', model: [chart: chart, user: user]
    }

    def confirmOrder() {

        def order = new Order()
        def amount = params.amount
        def chart = Chart.findById(params.id)
        order.chartId = params.id
        def total = 0
        for (int i = 0; i < chart.productList.size(); i++) {
            order.productList.put(chart.productList.get(i).id , Integer.parseInt(amount[i]))
            total += chart.productList.get(i).discountPrice * Integer.parseInt(amount[i])
        }
        order.total = total
        order.save()

        redirect action: 'index', params: [total: total, amount: amount, orderId: order.id]
    }

    def editOrder() {

        def order = Order.findById(params.id)
        order.delete()

        redirect(action: 'index')
    }

    def cancelOrder() {

        def order = Order.findById(params.id)
        def chart = Chart.findById(order.chartId)
        chart.delete()
        order.delete()
        redirect(uri: '/#')
    }

    def checkoutForm () {

        def user = this.getAuthenticatedUser()
        def order = Order.findById(params.id)
        def chart = Chart.findById(order.chartId)
        List<Product> productList = new ArrayList<>()
        def product = new Product()
        for (elem in order.productList) {
            product = Product.findById(elem.key)
            productList.add(product)
        }
        chart.delete()

        render view: 'checkout', model: [order: order, user: user, productList: productList]
    }

    def checkout() {

        def user = this.getAuthenticatedUser()
        def order = Order.findById(params.id)
        def product = new Product()
        StringBuilder listOfProductsForEmail = new StringBuilder("\n")
        for (elem in order.productList) {
            product = Product.findById(elem.key)
            product.stock -= elem.value
            product.save()
            listOfProductsForEmail.append(elem.value + " " + product.name  + "\n")
        }
        order.name = user.username
        order.clientId = user.id
        order.deliverOption = params.deliverOption
        order.paymentType = params.paymentType
        order.adress = params.adress
        order.status = "Aguardando pagamento"
        order.save()

        sendMail {
            from "testeestagiobit@gmail.com"
            to user.email
            subject "Nova compra " + order.id
            text ("Olá, " + user.username + "!" +
                    "\n" + "Sua compra de id: " + order.id + " está aguardando a confirmação de pagamento." + "\n" +
                    "Detalhes da compra: " + "\n" +
                    "Número do pedido: " + order.id + "\n" +
                    "Produtos: "  + listOfProductsForEmail +
                    "Valor total: " + order.total)
        }

        redirect(uri: '/#')
    }

    def removeItem() {

        def chart = Chart.findById(params.chartId)
        def product = new Product()
        for (elem in chart.productList) {
            if(elem.id.toString().equalsIgnoreCase(params.productId)) {
                product = elem
            }
        }
        chart.productList.remove(product)
        if (chart.productList.size() == 0) {
            chart.delete()
            redirect(uri: '/#')
        } else {
            chart.save()
            redirect(action: 'index')
        }
    }
}