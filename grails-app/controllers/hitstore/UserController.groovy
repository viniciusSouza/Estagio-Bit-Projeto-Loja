package hitstore

import hitStore.auth.*
import net.sf.ehcache.search.expression.Or

class UserController {

    def index() {}

    def save(User user) {
        def roleUser = Role.findByAuthority("ROLE_USER")
        user.authorities = [roleUser]
        user.save()

        redirect(action: 'index')
    }


    def orderList() {

        def user = this.getAuthenticatedUser()
        def orderList = Order.findAllByClientId(user.id)

        render view: 'orderList', model: [orderList: orderList]
    }
}
