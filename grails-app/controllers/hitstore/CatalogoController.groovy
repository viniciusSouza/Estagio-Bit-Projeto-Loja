package hitstore

import grails.plugin.springsecurity.annotation.Secured

class CatalogoController {

    def productService

    def index() {

        def productList = Product.list()

        render view: 'index', model: [productList: productList]
    }

    @Secured (['ROLE_USER'])
    def chart(Product product) {

        def user = this.getAuthenticatedUser()

        render view: 'chart' , model: [product: product, user: user]
    }

    def addToChart() {

        def product = Product.findById(params.id)
        def user = this.getAuthenticatedUser()
        def chart = Chart.findByClientId(user.id)
        if (chart != null) {
            if (!chart.productList.contains(product)) {
                chart.productList.add(product)
            }
        } else {
            chart = new Chart()
            chart.clientId = user.id
            chart.productList.add(product)
        }
        chart.save()

        redirect action: 'index'
    }

    def productDetails(Product product) {

        render view: 'productDetails', model: [product: product]
    }
}
